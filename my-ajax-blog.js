"use strict"

var ajaxRequest = $.ajax("data/my-ajax-blog.json");
ajaxRequest.done(function (data) {
    console.log(data);

    var dataHTML = buildHTML(data);
    $('.my-blog-post').html(dataHTML);
});

function buildHTML(orders) {

    var orderHTML = '';
    orders.forEach(function (order) {

        orderHTML += "<section>";
        orderHTML += "<dl>";
        orderHTML += "<dt>" + "Gamer" + "</dt>";
        orderHTML += "<dd>" + order.Gamer + "</dd>";
        orderHTML += "<dt>" + "Event" + "</dt>";
        orderHTML += "<dd>" + order.Event + "</dd>";
        orderHTML += "<dd>" + "Date" + "</dd>";
        orderHTML += "<dd>" + order.Date + "</dd>";
        orderHTML += "</dl>";
        orderHTML += "<section>";
    });
    return orderHTML;
}
$.document.getElementsByClassName("my-blog-post").css.innerHTML("<hr>")

